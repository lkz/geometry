let geomWASM;
let cacheUint32Mem;
let cacheFloat64Mem;

const uLen = 4; // uint32_t length
const dLen = 8; // double length


function proc_exit(arg) {}


function emscripten_notify_memory_growth(request) {
    cacheUint32Mem = new Uint32Array(geomWASM.memory.buffer);
    cacheFloat64Mem = new Float64Array(geomWASM.memory.buffer);
}

function getImports() {
    const imports = {};
    imports.wasi_snapshot_preview1 = {};
    imports.wasi_snapshot_preview1.proc_exit = proc_exit;
    imports.env = { emscripten_notify_memory_growth };
    // imports.env.emscripten_notify_memory_growth =
    //     emscripten_notify_memory_growth;
    return imports;
}

async function load(module, imports) {
    return await WebAssembly.instantiate(module, imports);
}

function finalizeInit(instance, module) {
    geomWASM = instance.exports;
    geomWASM.exactinit();
    emscripten_notify_memory_growth(null);
}

async function init() {
    const imports = getImports();
    const buffer = require("arraybuffer-loader!@qunhe/geometry/geometry.wasm");
    const { instance, module } = await load(buffer, imports);
    finalizeInit(instance, module);
}

init();

function assignArrToWASM(wasmArr, start, srcArr) {
    wasmArr.subarray(start, start + srcArr.length).set(srcArr);
}

function getArrFromWasm(wasmArr, start, len) {
    return Array.from(wasmArr.subarray(start, start + len));
}


export function makePolyhedralMesh(pointsData, separators, edgesData, axesData) {
    if (geomWASM === undefined) {
        return undefined;
    }
    // malloc 1
    const pointsPtr = geomWASM.malloc(dLen * pointsData.length);
    assignArrToWASM(cacheFloat64Mem, pointsPtr >> 3, pointsData);
    // malloc 2
    const separatorsPtr = geomWASM.malloc(uLen * separators.length);
    assignArrToWASM(cacheUint32Mem, separatorsPtr >> 2, separators);
    // malloc 3
    const edgesPtr = geomWASM.malloc(uLen * edgesData.length);
    assignArrToWASM(cacheUint32Mem, edgesPtr >> 2, edgesData);
    // malloc 4
    const axesPtr = geomWASM.malloc(dLen * axesData.length);
    assignArrToWASM(cacheFloat64Mem, axesPtr >> 3, axesData);
    // malloc 5
    const outPtsPtrRef = geomWASM.malloc(uLen); // malloc 6
    // malloc 7
    const outLoopsPtrRef = geomWASM.malloc(uLen); // malloc 8
    // malloc 9
    const outLoopSeparatorsPtrRef = geomWASM.malloc(uLen); // malloc 10
    // malloc 11
    const outFacesPtrRef = geomWASM.malloc(uLen); // malloc 12
    // malloc 13
    const outFaceSeparatorsPtrRef = geomWASM.malloc(uLen); // malloc 14
    // malloc 15
    const outAxesPtrRef = geomWASM.malloc(uLen); // malloc 16
    const numFaces = geomWASM.make_polyhedral_mesh(
        pointsPtr,
        pointsData.length / 3,
        edgesPtr,
        axesPtr,
        separatorsPtr,
        separators.length - 1,
        outPtsPtrRef,
        outLoopsPtrRef,
        outLoopSeparatorsPtrRef,
        outFacesPtrRef,
        outFaceSeparatorsPtrRef,
        outAxesPtrRef
    );
    // axes
    const outAxesPtr = cacheUint32Mem[outAxesPtrRef >> 2];
    const outAxes = getArrFromWasm(
        cacheFloat64Mem,
        outAxesPtr >> 3,
        numFaces * 6
    );
    // free 16
    geomWASM.free(outAxesPtr);
    // free 15
    geomWASM.free(outAxesPtrRef);
        
    // face separators
    const outFaceSeparatorsPtr = cacheUint32Mem[outFaceSeparatorsPtrRef >> 2];
    const outFaceSeparators = getArrFromWasm(
        cacheUint32Mem,
        outFaceSeparatorsPtr >> 2,
        numFaces + 1
    );
    // free 14
    geomWASM.free(outFaceSeparatorsPtr);
    // free 13
    geomWASM.free(outFaceSeparatorsPtrRef);

    // faces
    const outFacesPtr = cacheUint32Mem[outFacesPtrRef >> 2];
    const outFacess = getArrFromWasm(
        cacheUint32Mem,
        outFacesPtr >> 2,
        outFaceSeparators[numFaces]
    );
    const numLoops = Math.max(...outFacess) + 1;
    // free 12
    geomWASM.free(outFacesPtr);
    // free 11
    geomWASM.free(outFacesPtrRef);

    // loop separators
    const outLoopSeparatorsPtr = cacheUint32Mem[outLoopSeparatorsPtrRef >> 2];
    const outLoopSeparators = getArrFromWasm(
        cacheUint32Mem,
        outLoopSeparatorsPtr >> 2,
        numLoops + 1
    );
    // free 10
    geomWASM.free(outLoopSeparatorsPtr);
    // free 9
    geomWASM.free(outLoopSeparatorsPtrRef);

    // loops
    const outLoopsPtr = cacheUint32Mem[outLoopsPtrRef >> 2];
    const outLoops = getArrFromWasm(
        cacheUint32Mem,
        outLoopsPtr >> 2,
        outLoopSeparators[numLoops]
    );
    const outNPts = Math.max(...outLoops) + 1;
    // free 8
    geomWASM.free(outLoopsPtrRef);
    // free 7
    geomWASM.free(outLoopsPtr);

    // points
    const outPtsPtr = cacheUint32Mem[outPtsPtrRef >> 2];
    const outPts = getArrFromWasm(
        cacheFloat64Mem,
        outPtsPtr >> 3,
        outNPts * 3
    );
    // free 6
    geomWASM.free(outPtsPtrRef);
    // free 5
    geomWASM.free(outPtsPtr);
    // free 4
    geomWASM.free(axesPtr);
    // free 3
    geomWASM.free(separatorsPtr);
    // free 2
    geomWASM.free(edgesPtr);
    // free 1
    geomWASM.free(pointsPtr);

    return [outPts, outLoops, outLoopSeparators, outFacess, outFaceSeparators, outAxes];
}
