#include <utils/convex_hull.h>
#include <utils/minimum_area_box.h>

#include <algorithm>

using CVector2 = const Eigen::Map<const Eigen::Vector2d>;

MinimumAreaBox::Box::Box(const double* p, const double* q, const uint32_t idx) : index{idx, idx, idx, idx}, area{0} {
    axes[0] = (CVector2{q} - CVector2{p}).normalized();
    axes[1] = Eigen::Vector2d{-axes[0][1], axes[0][0]};
}

MinimumAreaBox::MinimumAreaBox(const double* p, std::vector<uint32_t>&& h) : points{p}, hull{std::move(h)} {}

MinimumAreaBox::Box MinimumAreaBox::compute_box(const uint32_t e0, const uint32_t e1) {
    Box box(point(hull[e0]), point(hull[e1]), e1);
    std::array<std::array<double, 2>, 4> support{{{0, 0}, {0, 0}, {0, 0}, {0, 0}}};
    CVector2 origin(point(hull[e1]));
    for (uint32_t i = 0; i < hull.size(); i++) {
        const auto diff = (CVector2{point(hull[i])} - origin).eval();
        std::array<double, 2> v{diff.dot(box.axes[0]), diff.dot(box.axes[1])};

        if (v[0] > support[1][0] || (v[0] == support[1][0] && v[1] > support[1][1])) {
            // right
            box.index[1] = i;
            support[1] = v;
        }

        if (v[1] > support[2][1] || (v[1] == support[2][1] && v[0] < support[2][0])) {
            // top
            box.index[2] = i;
            support[2] = v;
        }

        if (v[0] < support[3][0] || (v[0] == support[3][0] && v[1] < support[3][1])) {
            // left
            box.index[3] = i;
            support[3] = v;
        }
    }
    box.area = (support[1][0] - support[3][0]) * support[2][1];
    return box;
}

uint32_t MinimumAreaBox::compute_angles(const MinimumAreaBox::Box& box, std::pair<double, uint32_t>* angles) {
    uint32_t count = 0;
    for (uint32_t i = 0; i < 4; i++) {
        const auto j = (i + 1) % 4;
        if (box.index[i] != box.index[j]) {
            const auto& axis = box.axes[(~(i & 1)) & 1];
            const auto curr = box.index[i];
            auto next = curr + 1;
            if (next == hull.size()) {
                next = 0;
            }
            const auto v = (CVector2{point(hull[next])} - CVector2{point(hull[curr])}).normalized();
            angles[count++] = std::make_pair(std::fabs(v.dot(axis)), i);
        }
    }
    return count;
}

bool MinimumAreaBox::update_support(
    const std::pair<double, uint32_t>* angles, const uint32_t n_angles, std::vector<bool>& visited,
    MinimumAreaBox::Box& box
) {
    const auto min_angle_ptr =
        std::min_element(angles, angles + n_angles, [](const auto& a, const auto& b) { return a.first < b.first; });
    for (uint32_t i = 0; i < n_angles; i++) {
        auto& pair = angles[i];
        if (pair.first == min_angle_ptr->first) {
            if (++box.index[pair.second] == hull.size()) {
                box.index[pair.second] = 0;
            }
        }
    }
    auto bottom = box.index[min_angle_ptr->second];
    if (visited[bottom]) {
        return false;
    }
    visited[bottom] = true;
    std::rotate(box.index.begin(), box.index.begin() + min_angle_ptr->second, box.index.end());
    const auto e1 = box.index[0];
    const auto e0 = e1 == 0 ? static_cast<uint32_t>(hull.size() - 1) : e1 - 1;
    box.axes[0] = (CVector2{point(hull[e1])} - CVector2{point(hull[e0])}).normalized();
    box.axes[1] = Eigen::Vector2d{-box.axes[0][1], box.axes[0][0]};
    box.area = std::fabs(
        box.axes[0].dot(CVector2{point(hull[box.index[1]])} - CVector2{point(hull[box.index[3]])}) *
        box.axes[1].dot(CVector2{point(hull[box.index[0]])} - CVector2{point(hull[box.index[2]])})
    );
    return true;
}

void MinimumAreaBox::operator()() {
    std::vector<bool> visited(hull.size(), false);
    min_box = compute_box(0, 1);
    visited[1] = true;

    std::array<std::pair<double, uint32_t>, 4> angles;
    Box box = min_box;
    for (uint32_t _ = 0; _ < hull.size(); _++) {
        const auto n_angles = compute_angles(box, angles.data());
        if (n_angles == 0) {
            break;
        }
        if (!update_support(angles.data(), n_angles, visited, box)) {
            break;
        }
        if (box.area < min_box.area) {
            min_box = box;
        }
    }
}
