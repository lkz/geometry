#include <predicates/predicates.h>
#include <utils/convex_hull.h>

#include <algorithm>
#include <functional>
#include <numeric>

enum class Orientation {
    POSITIVE,         // counterclockwise
    NEGATIVE,         // clockwise
    COLLINEAR_LEFT,   // <p2, p0, p1>
    COLLINEAR_RIGHT,  // <p0, p1, p2>
    COLLINEAR_CONTAIN // <p0, p2, p1>
};
struct ConvexHull {
    const double* points;
    std::function<bool(const uint32_t, const uint32_t)> comparator;
    std::vector<uint32_t> hull, merged;

    ConvexHull(const double* p, const uint32_t n_points) : points{p} {
        hull.resize(n_points);
        std::iota(hull.begin(), hull.end(), 0);
        comparator = [this](const uint32_t i, const uint32_t j) {
            const auto* pi = point(i);
            const auto* pj = point(j);
            if (pi[0] < pj[0]) {
                return true;
            }
            if (pi[0] > pj[0]) {
                return false;
            }
            return pi[1] < pj[1];
        };
        std::sort(hull.begin(), hull.end(), comparator);
        const auto iter = std::unique(hull.begin(), hull.end(), [this](const uint32_t i, const uint32_t j) {
            const auto* pi = point(i);
            const auto* pj = point(j);
            return pi[0] == pj[0] && pi[1] == pj[1];
        });
        hull.erase(iter, hull.end());
        merged.resize(hull.size());
    }

    const double* point(const uint32_t idx) noexcept { return &points[idx << 1]; }

    Orientation orientation(const uint32_t q, const uint32_t p0, const uint32_t p1) {
        const auto* pq = point(q);
        const auto* pp0 = point(p0);
        const auto* pp1 = point(p1);
        const auto det = orient2d(pq, pp0, pp1);
        if (det > 0) {
            return Orientation::POSITIVE;
        } else if (det < 0) {
            return Orientation::NEGATIVE;
        }
        auto point_on_segment = [](const double* c, const double* a, const double* b) {
            // return true when c is on segment [a, b]
            return (a[0] < b[0] && a[0] < c[0] && c[0] < b[0]) || (b[0] < a[0] && b[0] < c[0] && c[0] < a[0]) ||
                   (a[1] < b[1] && a[1] < c[1] && c[1] < b[1]) || (b[1] < a[1] && b[1] < c[1] && c[1] < a[1]);
        };
        if (point_on_segment(pp0, pq, pp1)) {
            return Orientation::COLLINEAR_LEFT;
        } else if (point_on_segment(pp1, pp0, pq)) {
            return Orientation::COLLINEAR_RIGHT;
        } else {
            return Orientation::COLLINEAR_CONTAIN;
        }
    }

    void
    get_tangent(const uint32_t ls, const uint32_t le, const uint32_t rs, const uint32_t re, uint32_t& s, uint32_t& e) {
        const auto left_len = le - ls;
        const auto right_len = re - rs;
        const auto len = left_len + right_len;
        for (uint32_t _ = 0; _ < len; _++) {
            const auto left = hull[s];
            const auto right = hull[e];
            if (left_len > 1) {
                const auto idx = s > ls ? s - 1 : le - 1;
                const auto ori = orientation(right, hull[idx], left);
                if (ori == Orientation::NEGATIVE || ori == Orientation::COLLINEAR_RIGHT) {
                    s = idx;
                    continue;
                }
            }
            if (right_len > 1) {
                auto idx = e + 1;
                if (idx == re) {
                    idx = rs;
                }
                const auto ori = orientation(left, right, hull[idx]);
                if (ori == Orientation::NEGATIVE || ori == Orientation::COLLINEAR_LEFT) {
                    e = idx;
                    continue;
                }
            }
            break;
        }
    }

    void
    merge(const uint32_t ls, const uint32_t le, const uint32_t rs, const uint32_t re, uint32_t& start, uint32_t& end) {
        const auto left_len = le - ls;
        const auto right_len = re - rs;

        // right-most point of the left hull
        uint32_t left_right{ls};
        for (uint32_t i = ls + 1; i < le; i++) {
            if (comparator(hull[left_right], hull[i])) {
                left_right = i;
            }
        }

        // left-most point of the right hull
        uint32_t right_left{rs};
        for (uint32_t i = rs + 1; i < re; i++) {
            if (comparator(hull[i], hull[right_left])) {
                right_left = i;
            }
        }

        // get the lower tangent to hulls
        // ll = lower-left, lr = lower-right
        uint32_t ll{left_right}, lr{right_left};
        get_tangent(ls, le, rs, re, ll, lr);

        // get the upper tangent to hulls
        // ul = upper-left, ur = upper-right
        uint32_t ul{left_right}, ur{right_left};
        get_tangent(rs, re, ls, le, ur, ul);

        uint32_t count = 0, idx = ul;
        for (uint32_t _ = 0; _ < left_len; _++) {
            merged[count++] = hull[idx];
            if (idx == ll) {
                break;
            }
            idx += 1;
            if (idx == le) {
                idx = ls;
            }
        }

        idx = lr;
        for (uint32_t _ = 0; _ < right_len; _++) {
            merged[count++] = hull[idx];
            if (idx == ur) {
                break;
            }
            idx += 1;
            if (idx == re) {
                idx = rs;
            }
        }
        std::copy(merged.begin(), merged.begin() + count, hull.begin() + ls);
        start = ls;
        end = ls + count;
    }

    void get_hull(uint32_t& start, uint32_t& end) {
        const auto n_points = end - start;
        if (n_points < 2) {
            return;
        }
        uint32_t left_end = start + (n_points >> 1);
        uint32_t right_start = left_end;
        get_hull(start, left_end);
        get_hull(right_start, end);
        merge(start, left_end, right_start, end, start, end);
    }

    void operator()(std::vector<double>& hull_data) {
        uint32_t start{0}, end{static_cast<uint32_t>(hull.size())};
        get_hull(start, end);
        hull_data.resize(end << 1);
        auto* tar = hull_data.data();
        for (uint32_t i = 0; i < end; i++) {
            const auto* src = point(hull[i]);
            tar[0] = src[0];
            tar[1] = src[1];
            tar += 2;
        }
    }
};

void convex_hull(const double* points, const uint32_t n_points, std::vector<double>& hull) {
    ConvexHull ch(points, n_points);
    ch(hull);
}
