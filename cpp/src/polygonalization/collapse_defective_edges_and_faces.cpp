#include <polygonalization/collapse_defective_edges_and_faces.h>
#include <triangle/triangulate_polygon.h>
#include <utils/combining_hash_functions.h>
#include <utils/heap.h>


#include <Eigen/Dense>

#include <algorithm>
#include <array>
#include <iterator>
#include <stack>
#include <unordered_map>

using Vec3 = Eigen::Map<const Eigen::Vector3d>;

static inline const double* point3(const double* data, const uint32_t idx) noexcept { return &data[idx * 3]; }

struct Edge {
    std::array<uint32_t, 2> verts;
    std::vector<uint32_t> loops;
    Edge(const uint32_t va, const uint32_t vb, const uint32_t lid) : verts{va, vb}, loops{lid} {}
    uint32_t va() const noexcept { return verts[0]; }
    uint32_t& va() noexcept { return verts[0]; }
    uint32_t vb() const noexcept { return verts[1]; }
    uint32_t& vb() noexcept { return verts[1]; }
};

static inline int half_edge_index(const uint32_t eid, const bool reversed) noexcept {
    return reversed ? -static_cast<int>(eid + 1) : static_cast<int>(eid + 1);
}

static inline uint32_t edge_index(const int hid) noexcept { return static_cast<uint32_t>(std::abs(hid) - 1); }

static inline int make_half_edge(
    const uint32_t va, const uint32_t vb, const uint32_t fid, std::vector<Edge>& edges,
    std::unordered_map<std::pair<uint32_t, uint32_t>, uint32_t>& edge_map
) noexcept {
    auto key = va < vb ? std::make_pair(va, vb) : std::make_pair(vb, va);
    const auto iter = edge_map.find(key);
    if (iter != edge_map.end()) {
        auto& edge = edges[iter->second];
        edge.loops.emplace_back(fid);
        return half_edge_index(iter->second, edge.verts[0] == vb);
    } else {
        const auto eid = static_cast<uint32_t>(edges.size());
        edges.emplace_back(va, vb, fid);
        edge_map.emplace(std::move(key), eid);
        return half_edge_index(eid, false);
    }
}

static std::vector<std::array<int, 3>> make_triangle_mesh(
    std::vector<double>& points, std::vector<double>& axes, std::vector<std::vector<uint32_t>>& loops,
    std::vector<std::vector<uint32_t>>& faces, std::vector<Edge>& edges, std::vector<uint32_t>& triangle_face_indices
) {
    std::vector<std::array<int, 3>> triangles;
    std::unordered_map<std::pair<uint32_t, uint32_t>, uint32_t> edge_map;
    for (uint32_t fid = 0; fid < faces.size(); fid++) {
        std::vector<uint32_t> segments;
        for (const auto& lid : faces[fid]) {
            const auto& loop = loops[lid];
            for (uint32_t i = 0, len = static_cast<uint32_t>(loop.size()); i < loop.size(); i++) {
                segments.emplace_back(loop[i]);
                segments.emplace_back(loop[(i + 1) % len]);
            }
        }
        uint32_t n_triangles{0};
        const auto poly_triangles = triangulate_polygon(
            points.data(), segments.data(), static_cast<uint32_t>(segments.size() >> 1),
            &points[loops[faces[fid][0]][0] * 3], &axes[fid * 6], &axes[fid * 6 + 3], &n_triangles
        );
        for (uint32_t i = 0; i < n_triangles; i++) {
            const auto indices = &poly_triangles[i * 3];
            std::array<int, 3> tri;
            const auto tri_id = static_cast<uint32_t>(triangles.size());
            for (uint32_t j = 0; j < 3; j++) {
                const uint32_t k = j == 2 ? 0 : (j + 1);
                tri[j] = make_half_edge(indices[j], indices[k], tri_id, edges, edge_map);
            }
            triangles.emplace_back(std::move(tri));
            triangle_face_indices.emplace_back(fid);
        }
        delete[] poly_triangles;
    }
    return triangles;
}

static inline std::pair<uint32_t, double> min_height(
    const double* points, const int* tri, const std::vector<Edge>& edges, const std::vector<double>& edge_lens
) noexcept {
    uint32_t verts[3];
    uint32_t eids[3];
    uint32_t max_edge_idx{0};
    for (uint32_t i = 0; i < 3; i++) {
        eids[i] = edge_index(tri[i]);
        const auto& edge = edges[eids[i]];
        verts[i] = tri[i] > 0 ? edge.va() : edge.vb();
        if (edge_lens[eids[i]] > edge_lens[eids[max_edge_idx]]) {
            max_edge_idx = i;
        }
    }

    const Vec3 p0{point3(points, verts[0])};
    const Vec3 p1{point3(points, verts[1])};
    const Vec3 p2{point3(points, verts[2])};
    const auto square_area = (p1 - p0).cross(p2 - p0).squaredNorm();
    return std::make_pair(max_edge_idx, square_area / edge_lens[eids[max_edge_idx]]);
}

struct TriangleHeapInfo {
    std::vector<uint32_t> max_edge_indices;
    std::vector<double> heights;
    std::vector<uint32_t> positions;

    bool less(const uint32_t a, const uint32_t b) const noexcept { return heights[a] < heights[b]; }
    void set_heap_position(const uint32_t entry, const uint32_t pos) noexcept { positions[entry] = pos; }

    uint32_t get_heap_position(const uint32_t entry) const noexcept { return positions[entry]; }
};

static inline double edge_length(const double* points, const Edge& edge) noexcept {
    Vec3 p0{point3(points, edge.va())};
    Vec3 p1{point3(points, edge.vb())};
    return (p1 - p0).squaredNorm();
}

static inline std::vector<uint32_t> vert_edges(
    const int hid, const std::vector<Edge>& edges, const std::vector<std::array<int, 3>>& triangles,
    std::vector<bool>& triangle_visit, std::vector<bool>& edge_visit
) noexcept {
    uint32_t eid = edge_index(hid);
    const auto vert = hid > 0 ? edges[eid].va() : edges[eid].vb();
    std::vector<uint32_t> visit_triangles;
    std::vector<uint32_t> visit_edges{eid};
    std::stack<uint32_t> stack; // edge stack
    edge_visit[eid] = true;
    stack.emplace(eid);
    while (!stack.empty()) {
        eid = stack.top();
        stack.pop();
        for (const auto tri_idx : edges[eid].loops) {
            if (triangle_visit[tri_idx]) {
                continue;
            }
            triangle_visit[tri_idx] = true;
            visit_triangles.emplace_back(tri_idx);

            for (const auto h : triangles[tri_idx]) {
                eid = edge_index(h);
                if (edge_visit[eid]) {
                    continue;
                }
                const auto& edge = edges[eid];
                if (edge.va() != vert && edge.vb() != vert) {
                    continue;
                }
                edge_visit[eid] = true;
                visit_edges.emplace_back(eid);
                stack.emplace(eid);
            }
        }
    }

    for (const auto tri_idx : std::move(visit_triangles)) {
        triangle_visit[tri_idx] = false;
    }

    for (const auto e : visit_edges) {
        edge_visit[e] = false;
    }

    return visit_edges;
}

static inline void update_triangles_on_heap(
    HeapT<uint32_t, TriangleHeapInfo>& heap, const std::vector<uint32_t>& triangle_indices, const double* points,
    const std::vector<std::array<int, 3>>& triangles, const std::vector<Edge>& edges,
    const std::vector<double>& edge_lens, const double square_tol
) noexcept {
    for (const auto tri_idx : triangle_indices) {
        auto& interface = heap.get_interface();
        std::tie(interface.max_edge_indices[tri_idx], interface.heights[tri_idx]) =
            min_height(points, triangles[tri_idx].data(), edges, edge_lens);
        if (heap.get_interface().heights[tri_idx] < square_tol) {
            heap.update(tri_idx);
        } else {
            heap.remove(tri_idx);
        }
    }
}

static void collapse_edge(
    const double* points, const int hid, std::vector<std::array<int, 3>>& triangles, std::vector<Edge>& edges,
    std::vector<bool>& triangle_visit, std::vector<bool>& edge_visit, std::vector<bool>& triangle_remove,
    HeapT<uint32_t, TriangleHeapInfo>& heap, std::vector<double>& edge_lens, const double square_tol
) {
    const auto collapse_eid = edge_index(hid);
    const auto& edge_to_collapse = edges[collapse_eid];
    const auto collapsed_vert = hid > 0 ? edge_to_collapse.va() : edge_to_collapse.vb();
    const auto target_vert = hid > 0 ? edge_to_collapse.vb() : edge_to_collapse.va();
    const std::vector<uint32_t>& remove_triangles = edges[collapse_eid].loops;
    std::vector<uint32_t> modify_edges = vert_edges(hid, edges, triangles, triangle_visit, edge_visit);

    for (const auto tri_idx : remove_triangles) {
        heap.remove(tri_idx);
    }
    std::vector<uint32_t> update_triangles;
    for (const auto tri_idx : remove_triangles) {
        triangle_remove[tri_idx] = true;
        const auto& tri = triangles[tri_idx];

        uint32_t e0 = 0;
        for (; e0 < 3; e0++) {
            if (edge_index(tri[e0]) == collapse_eid) {
                break;
            }
        }
        uint32_t e1 = (e0 + 1) % 3;
        uint32_t e2 = (e1 + 1) % 3;
        int remove_hid, keep_hid;
        const auto& edge1 = edges[edge_index(tri[e1])];
        if (edge1.va() == collapsed_vert || edge1.vb() == collapsed_vert) {
            remove_hid = tri[e1];
            keep_hid = tri[e2];
        } else {
            remove_hid = tri[e2];
            keep_hid = tri[e1];
        }
        // remove tri_idx from edge's neighbor list
        for (const auto h : {remove_hid, keep_hid}) {
            auto& edge = edges[edge_index(h)];
            const auto iter = std::remove(edge.loops.begin(), edge.loops.end(), tri_idx);
            edge.loops.erase(iter, edge.loops.end());
        }
        // replace remove_hid with keep_hid
        for (const auto nei_tri_idx : edges[edge_index(remove_hid)].loops) {
            auto& nei_tri = triangles[nei_tri_idx];
            const auto iter = std::find_if(nei_tri.begin(), nei_tri.end(), [remove_hid](const int h) {
                return h == remove_hid || (h + remove_hid == 0);
            });
            if (*iter == remove_hid) {
                *iter = -keep_hid;
            } else {
                *iter = keep_hid;
            }
            if (!triangle_visit[nei_tri_idx]) {
                triangle_visit[nei_tri_idx] = true;
                update_triangles.emplace_back(nei_tri_idx);
            }
        }
        // assign the neighbors of remove-edge to keep edge
        auto& remove_edge_tris = edges[edge_index(remove_hid)].loops;
        std::copy(
            remove_edge_tris.begin(), remove_edge_tris.end(), std::back_inserter(edges[edge_index(keep_hid)].loops)
        );
    }

    // first edge have collapsed
    for (uint32_t i = 1; i < modify_edges.size(); i++) {
        const auto eid = modify_edges[i];
        auto& edge = edges[eid];
        if (edge.va() == collapsed_vert) {
            edge.va() = target_vert;
        } else if (edge.vb() == collapsed_vert) {
            edge.vb() = target_vert;
        }
        edge_lens[eid] = edge_length(points, edge);
    }
    for (const auto tri_idx : update_triangles) {
        triangle_visit[tri_idx] = false;
    }
    update_triangles_on_heap(heap, update_triangles, points, triangles, edges, edge_lens, square_tol);
}

static void collapse_vertex(
    const uint32_t collapse_triangle_idx, const uint32_t idx_on_triangle, double* points, const double* target_point,
    std::vector<std::array<int, 3>>& triangles, std::vector<uint32_t>& triangle_face_indices, std::vector<Edge>& edges,
    std::vector<double>& edge_lens, std::vector<bool>& triangle_visit, std::vector<bool>& edge_visit,
    std::vector<bool>& triangle_remove, HeapT<uint32_t, TriangleHeapInfo>& heap, const double square_tol
) noexcept {
    const auto& collapse_triangle = triangles[collapse_triangle_idx];
    const auto h0 = collapse_triangle[idx_on_triangle];
    const auto h1 = collapse_triangle[(idx_on_triangle + 1) % 3];
    const auto h2 = collapse_triangle[(idx_on_triangle + 2) % 3];
    const auto e0 = edge_index(h0);
    const auto collapse_vertex = h1 > 0 ? edges[edge_index(h1)].vb() : edges[edge_index(h1)].va();
    std::vector<uint32_t> modify_edges = vert_edges(h2, edges, triangles, triangle_visit, edge_visit);
    std::vector<uint32_t> update_triangles;

    // remove collapse-triangle
    for (const auto hid : collapse_triangle) {
        auto& faces = edges[edge_index(hid)].loops;
        const auto iter = std::remove(faces.begin(), faces.end(), collapse_triangle_idx);
        faces.erase(iter, faces.end());
        std::copy(faces.begin(), faces.end(), std::back_inserter(update_triangles));
    }
    triangle_remove[collapse_triangle_idx] = true;

    // update connection relation
    {
        const auto& e0_faces = edges[e0].loops;
        std::copy(e0_faces.begin(), e0_faces.end(), std::back_inserter(edges[edge_index(h2)].loops));
    }
    // split edge h0 and form new triangles
    for (const auto tid : edges[e0].loops) {
        auto& triangle = triangles[tid];
        const auto idx = static_cast<uint32_t>(std::distance(
            triangle.begin(),
            std::find_if(
                triangle.begin(), triangle.end(), [e0](const auto hid) noexcept { return edge_index(hid) == e0; }
            )
        ));
        auto& next_hid = triangle[(idx + 1) % 3];
        const auto new_eid = static_cast<uint32_t>(edges.size());
        const auto new_hid = static_cast<int>(new_eid + 1);
        edges.emplace_back(
            collapse_vertex, next_hid > 0 ? edges[edge_index(next_hid)].vb() : edges[edge_index(next_hid)].va(), tid
        );
        edge_lens.emplace_back(0.0);

        const auto new_tid = static_cast<uint32_t>(triangles.size());
        edges.back().loops.emplace_back(new_tid);

        modify_edges.emplace_back(new_eid);
        update_triangles.emplace_back(new_tid);

        heap.get_interface().heights.emplace_back(0.0);
        heap.get_interface().max_edge_indices.emplace_back(0);
        heap.get_interface().positions.emplace_back(TriFace::INVALID);

        if (triangle[idx] == h0) {
            std::array<int, 3> new_tri{-h1, next_hid, -new_hid};
            triangle[idx] = -h2;
            next_hid = new_hid;
            triangles.emplace_back(std::move(new_tri));
        } else {
            auto& prev_hid = triangle[(idx + 2) % 3];
            std::array<int, 3> new_tri{prev_hid, h1, new_hid};
            triangle[idx] = h2;
            prev_hid = -new_hid;
            triangles.emplace_back(std::move(new_tri));
        }
        triangle_face_indices.emplace_back(triangle_face_indices[tid]);
    }
    // pull collapse point to e0
    {
        double* p = &points[collapse_vertex * 3];
        p[0] = target_point[0];
        p[1] = target_point[1];
        p[2] = target_point[2];
    }
    for (const auto eid : modify_edges) {
        edge_lens[eid] = edge_length(points, edges[eid]);
    }
    update_triangles_on_heap(heap, update_triangles, points, triangles, edges, edge_lens, square_tol);
}

inline double loop_area(const double* points, std::vector<uint32_t>& verts) noexcept {
    const Vec3 p0{point3(points, verts[0])};
    double area = 0.0;
    Eigen::Vector3d normal;
    bool has_normal = false;
    for (uint32_t i = 2; i < verts.size(); i++) {
        const Vec3 p1{point3(points, verts[i - 1])};
        const Vec3 p2{point3(points, verts[i])};
        const auto n = (p1 - p0).cross(p2 - p0);
        const auto norm = n.norm();
        if (!has_normal && norm != 0.0) {
            normal = n;
        }
        area += n.dot(normal) > 0.0 ? norm : -norm;
    }
    return std::fabs(area);
}

static inline void remove_unused_vertices(
    std::vector<double>& points, const std::vector<bool>& visit, std::vector<std::vector<uint32_t>>& loops
) noexcept {
    std::vector<uint32_t> vmap(visit.size());
    uint32_t count{0};
    for (uint32_t i = 0; i < visit.size(); i++) {
        if (!visit[i]) {
            continue;
        }
        vmap[i] = count;
        if (i != count) {
            const double* src = &points[i * 3];
            double* tar = &points[count * 3];
            tar[0] = src[0];
            tar[1] = src[1];
            tar[2] = src[2];
        }
        count += 1;
    }
    points.resize(count * 3);

    for (auto& vertices : loops) {
        for (uint32_t i = 0; i < vertices.size(); i++) {
            vertices[i] = vmap[vertices[i]];
        }
    }
}

static void collapse_small_faces(
    std::vector<double>& points, std::vector<double>& axes, std::vector<std::vector<uint32_t>>& loops,
    std::vector<std::vector<uint32_t>>& faces, const double tol
) {
    std::vector<Edge> edges;
    std::vector<uint32_t> triangle_face_indices;
    auto triangles = make_triangle_mesh(points, axes, loops, faces, edges, triangle_face_indices);
    std::vector<double> edge_lens(edges.size());
    for (uint32_t i = 0; i < edges.size(); i++) {
        edge_lens[i] = edge_length(points.data(), edges[i]);
    }

    HeapT<uint32_t, TriangleHeapInfo> heap(TriangleHeapInfo{});

    auto& interface = heap.get_interface();
    interface.max_edge_indices.reserve(triangles.size());
    interface.heights.reserve(triangles.size());
    interface.positions.reserve(triangles.size());
    const auto square_tol = tol * tol;
    {
        for (uint32_t i = 0; i < triangles.size(); i++) {
            const auto pair = min_height(points.data(), triangles[i].data(), edges, edge_lens);
            interface.max_edge_indices.emplace_back(pair.first);
            interface.heights.emplace_back(pair.second);
            interface.positions.emplace_back(TriFace::INVALID);
            if (interface.heights.back() < square_tol) {
                heap.insert(i);
            }
        }
    }

    if (heap.empty()) {
        return;
    }

    uint32_t tri_idx;
    std::vector<bool> edge_visit(edges.size(), false);
    std::vector<bool> triangle_visit(triangles.size(), false);
    std::vector<bool> triangle_remove(triangles.size(), false);
    while (!heap.empty() && interface.heights[tri_idx = heap.front()] < square_tol) {
        heap.pop_front();
        const auto& triangle = triangles[tri_idx];
        uint32_t vs[3];
        for (uint32_t i = 0, j = interface.max_edge_indices[tri_idx]; i < 3; i++, j = (j + 1) % 3) {
            const auto& edge = edges[edge_index(triangle[j])];
            vs[i] = triangle[j] > 0 ? edge.va() : edge.vb();
        }
        const auto p0 = Vec3{point3(points.data(), vs[0])};
        const auto p1 = Vec3{point3(points.data(), vs[1])};
        const auto p2 = Vec3{point3(points.data(), vs[2])};
        const auto diff = (p1 - p0).eval();
        const auto s1 = (p2 - p0).dot(diff);
        const auto s2 = (p1 - p2).dot(diff);
        const auto t1 = s1 / (s1 + s2);
        const auto t2 = 1.0 - t1;
        const auto mid_pt = (t2 * p0 + t1 * p1).eval();
        const auto square_dist = t1 < t2 ? (mid_pt - p0).squaredNorm() : (mid_pt - p1).squaredNorm();
        if (square_dist < square_tol) {
            const auto hid = t1 < t2 ? triangle[(interface.max_edge_indices[tri_idx] + 2) % 3]
                                     : (-triangle[(interface.max_edge_indices[tri_idx] + 1) % 3]);
            collapse_edge(
                points.data(), hid, triangles, edges, triangle_visit, edge_visit, triangle_remove, heap, edge_lens,
                square_tol
            );
        } else {
            collapse_vertex(
                tri_idx, interface.max_edge_indices[tri_idx], points.data(), mid_pt.data(), triangles,
                triangle_face_indices, edges, edge_lens, triangle_visit, edge_visit, triangle_remove, heap, square_tol
            );
        }
    }
    std::vector<std::vector<uint32_t>> face_triangles(faces.size());
    for (tri_idx = 0; tri_idx < triangles.size(); tri_idx++) {
        if (!triangle_remove[tri_idx]) {
            face_triangles[triangle_face_indices[tri_idx]].emplace_back(tri_idx);
        }
    }

    {
        const auto iter =
            std::remove_if(face_triangles.begin(), face_triangles.end(), [](const auto& ele) { return ele.empty(); });
        face_triangles.erase(iter, face_triangles.end());
    }

    loops.clear();
    std::vector<bool> vert_visit(points.size() / 3);
    for (uint32_t i = 0; i < face_triangles.size(); i++) {
        std::unordered_map<uint32_t, int> edge_map;
        const auto& tri_group = face_triangles[i];
        for (const auto tid : tri_group) {
            for (const auto hid : triangles[tid]) {
                const auto eid = edge_index(hid);
                const auto iter = edge_map.find(eid);
                if (iter != edge_map.end()) {
                    iter->second += hid < 0 ? -1 : 1;
                } else {
                    edge_map.emplace(eid, hid < 0 ? -1 : 1);
                }
            }
        }
        std::vector<int> half_edges;
        std::unordered_map<uint32_t, int> vert_out_edge_map;
        for (const auto& pair : edge_map) {
            if (pair.second == 0) {
                continue;
            }
            const auto eid = pair.first;
            const auto hid = static_cast<int>(eid + 1) * (pair.second < 0 ? -1 : 1);
            half_edges.emplace_back(hid);
            const auto& edge = edges[eid];
            const auto va = pair.second < 0 ? edge.vb() : edge.va();
            vert_out_edge_map.emplace(va, hid);
        }

        const auto end_vertex = [&edges](const int hid) {
            const auto eid = edge_index(hid);
            const auto& edge = edges[eid];
            return hid < 0 ? edge.va() : edge.vb();
        };

        std::vector<uint32_t> loop_indices;
        for (const auto hid : half_edges) {
            const auto eid = edge_index(hid);
            if (edge_visit[eid]) {
                continue;
            }
            edge_visit[eid] = true;
            std::vector<int> outlines{hid};
            do {
                const auto vb = end_vertex(outlines.back());
                const auto next_hid = vert_out_edge_map[vb];
                const auto next_eid = edge_index(next_hid);
                if (edge_visit[next_eid] != 0) {
                    break;
                } else {
                    outlines.emplace_back(next_hid);
                    edge_visit[next_eid] = 1;
                }
            } while (true);
            std::vector<uint32_t> loop_verts(outlines.size());
            for (uint32_t j = 0; j < outlines.size(); j++) {
                const auto v = end_vertex(outlines[j]);
                if (!vert_visit[v]) {
                    vert_visit[v] = true;
                }
                loop_verts[j] = v;
            }
            loop_indices.emplace_back(static_cast<uint32_t>(loops.size()));
            loops.emplace_back(std::move(loop_verts));
        }

        for (const auto hid : half_edges) {
            edge_visit[edge_index(hid)] = false;
        }

        if (loop_indices.size() > 0) {
            double max_area = loop_area(points.data(), loops[loop_indices[0]]);
            uint32_t max_idx = 0;
            for (uint32_t j = 1; j < loop_indices.size(); j++) {
                const auto area = loop_area(points.data(), loops[loop_indices[j]]);
                if (area > max_area) {
                    max_area = area;
                    max_idx = j;
                }
            }
            if (max_idx != 0) {
                std::swap(loop_indices[0], loop_indices[max_idx]);
            }
        }
        faces[i] = std::move(loop_indices);
        const auto fid = triangle_face_indices[tri_group[0]];
        if (i != fid) {
            std::copy(axes.begin() + fid * 3, axes.begin() + (fid * 3 + 6), axes.begin() + i * 3);
        }
    }
    remove_unused_vertices(points, vert_visit, loops);
}

static inline decltype(auto)
make_loop_edges(const std::vector<std::vector<uint32_t>>& loops, std::vector<Edge>& edges) noexcept {
    std::vector<std::vector<int>> face_loops(loops.size());
    std::unordered_map<std::pair<uint32_t, uint32_t>, uint32_t> edge_map;
    for (uint32_t lid = 0; lid < loops.size(); lid++) {
        const auto& loop = loops[lid];
        std::vector<int> loop_edges;
        loop_edges.reserve(loop.size());
        for (uint32_t i{0}, len{static_cast<uint32_t>(loop.size())}; i < len; i++) {
            loop_edges.emplace_back(make_half_edge(loop[i], loop[(i + 1) % len], lid, edges, edge_map));
        }
        face_loops[lid] = std::move(loop_edges);
    }
    return face_loops;
}

static void merge_colinear_edges(
    const std::vector<double>& points, std::vector<Edge>& edges, std::vector<std::vector<int>>& loops, const double tol
) {
    for (auto& half_edges : loops) {
        // don't merge triangle's edges
        if (half_edges.size() < 4) {
            continue;
        }
        uint32_t start_idx = 0;
        const auto colinear = [&half_edges, &start_idx, &edges, &points, tol](const int hid2) {
            // two half edges have opposite directions
            const auto hid1 = half_edges[start_idx];
            if ((hid1 ^ hid2) < 0) {
                return false;
            }

            const auto& e1 = edges[edge_index(hid1)];
            const auto& e2 = edges[edge_index(hid2)];
            if (e1.loops.size() != e2.loops.size()) {
                return false;
            }

            // judge if the faces incident to e1 are consistent to
            // the faces incident to e2. If consistent, they have to
            // have same order according to our loop-making rule.
            for (uint32_t i = 0; i < e1.loops.size(); i++) {
                if (e1.loops[i] != e2.loops[i]) {
                    return false;
                }
            }

            const auto va = hid1 < 0 ? e1.vb() : e1.va();
            const auto vb = hid2 < 0 ? e2.va() : e2.vb();
            const auto vc = hid2 < 0 ? e2.vb() : e2.va();

            using CVec3 = const Eigen::Map<const Eigen::Vector3d>;
            CVec3 pa(&points[va * 3]);
            CVec3 pb(&points[vb * 3]);
            CVec3 pc(&points[vc * 3]);
            const auto v1 = pb - pa;
            const auto v2 = pc - pa;
            const auto dist = v1.cross(v2).norm() / v1.norm();
            return dist < tol;
        };
        const auto discard_edges = [&loops](const uint32_t eid, const int n_ele, std::vector<uint32_t>& loop_indices) {
            for (const uint32_t lid : loop_indices) {
                auto& loop = loops[lid];
                // have to be found
                auto pos = static_cast<uint32_t>(std::distance(
                    loop.begin(),
                    std::find_if(loop.begin(), loop.end(), [eid](const int hid) { return edge_index(hid) == eid; })
                ));
                const auto n_removed = static_cast<uint32_t>(std::abs(n_ele));
                if (loop[pos] > 0 ^ n_ele > 0) {
                    // backward deletion
                    pos += 1;
                    if (n_removed > pos) {
                        loop.resize(loop.size() - (n_removed - pos));
                        loop.erase(loop.begin(), loop.begin() + pos);
                    } else {
                        loop.erase(loop.begin() + (pos - n_removed), loop.begin() + pos);
                    }
                } else {
                    // forward deletion
                    if (pos + n_removed > loop.size()) {
                        const auto n = pos + n_removed - static_cast<decltype(pos)>(loop.size());
                        loop.resize(pos);
                        loop.erase(loop.begin(), loop.begin() + n);
                    } else {
                        loop.erase(loop.begin() + pos, loop.begin() + pos + n_removed);
                    }
                }
            }
        };
        for (uint32_t i = 1; i < half_edges.size();) {
            const auto hb = half_edges[i];
            if (colinear(hb)) {
                const auto ha = half_edges[start_idx];
                auto& ea = edges[edge_index(ha)];
                const auto& eb = edges[edge_index(hb)];
                auto& v1 = ha < 0 ? ea.va() : ea.vb();
                v1 = hb < 0 ? eb.va() : eb.vb();
                i += 1;
            } else {
                if (start_idx + 1 < i) {
                    const auto pos = start_idx + 1;
                    const auto hid = half_edges[pos];
                    const auto eid = edge_index(hid);
                    discard_edges(eid, static_cast<int>(i - pos) * (hid > 0 ? 1 : -1), edges[eid].loops);
                }
                start_idx = i;
                i += 1;
            }
        }
        if (start_idx + 1 < half_edges.size()) {
            const auto pos = start_idx + 1;
            const auto hid = half_edges[pos];
            const auto eid = edge_index(hid);
            discard_edges(eid, static_cast<int>(half_edges.size() - pos) * (hid > 0 ? 1 : -1), edges[eid].loops);
        }
    }
}

void collapse_defective_edges_and_faces(
    std::vector<double>& points, std::vector<double>& axes, std::vector<std::vector<uint32_t>>& loops,
    std::vector<std::vector<uint32_t>>& faces, const double tol
) {
    collapse_small_faces(points, axes, loops, faces, tol);
    std::vector<Edge> edges;
    auto loop_edges = make_loop_edges(loops, edges);
    merge_colinear_edges(points, edges, loop_edges, tol);

    const auto n_points = static_cast<uint32_t>(points.size() / 3);
    std::vector<bool> vert_visited(n_points, false);
    for (uint32_t i = 0; i < loops.size(); i++) {
        loops[i].resize(loop_edges[i].size());
        std::transform(
            loop_edges[i].begin(), loop_edges[i].end(), loops[i].begin(),
            [&edges, &vert_visited](const auto hid) {
                const auto& edge = edges[edge_index(hid)];
                const auto v = hid < 0 ? edge.vb() : edge.va();
                vert_visited[v] = true;
                return v;
            }
        );
    }
    remove_unused_vertices(points, vert_visited, loops);
}
