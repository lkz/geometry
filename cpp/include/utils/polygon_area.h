#pragma once

#include <Eigen/Dense>

inline double polygon_area(
    const double* points, const double* x_axis, const double* y_axis, const std::vector<uint32_t>& polys
) noexcept {
    using Vec3 = Eigen::Map<const Eigen::Vector3d>;
    const Vec3 x{x_axis};
    const Vec3 y{y_axis};
    const Vec3 o{&points[polys[0] * 3]};
    double area = 0.0;
    for (uint32_t i = 2; i < polys.size(); i++) {
        const Vec3 a{&points[polys[i - 1] * 3]};
        const Vec3 b{&points[polys[i] * 3]};
        const auto cax = (a - o).dot(x);
        const auto cay = (a - o).dot(y);
        const auto cbx = (b - o).dot(x);
        const auto cby = (b - o).dot(y);
        area += cax * cby - cay * cbx;
    }
    return area;
}
