#pragma once

#include <cstdint>
#include <triangle/tetrahedron.h>
#include <vector>

template <typename T, typename Interface>
concept HeapInterfaceT = requires(const T& t, Interface& i, const uint32_t idx) {
    { i.less(t, t) } -> std::same_as<bool>;
    { i.set_heap_position(t, idx) } -> std::same_as<void>;
    { i.get_heap_position(t) } -> std::convertible_to<uint32_t>;
};

template <typename Entry, typename Interface>
requires HeapInterfaceT<Entry, Interface>
class HeapT : private std::vector<Entry> {
  private:
    using Base = std::vector<Entry>;
    Interface interface;

  public:
    HeapT() : Base() {}
    HeapT(Interface&& i) : interface { std::move(i) } {
    }
    ~HeapT() {}

    const Entry& front() const noexcept { return Base::front(); }
    Interface& get_interface() noexcept { return interface; }

    bool empty() const noexcept { return Base::empty(); }

    void insert(const Entry& h) noexcept {
        Base::emplace_back(h);
        upheap(static_cast<uint32_t>(Base::size() - 1));
    }

    void reset_heap_position(const Entry& h) noexcept { interface.set_heap_position(h, TriFace::INVALID); }

    void pop_front() noexcept {
        reset_heap_position(front());
        if (Base::size() > 1) {
            set_entry(0, entry(static_cast<uint32_t>(Base::size() - 1)));
            Base::pop_back();
            downheap(0);
        } else {
            Base::pop_back();
        }
    }
        
    void update(const Entry& h) noexcept {
        const auto pos = interface.get_heap_position(h);
        if (pos == TriFace::INVALID) {
            insert(h);
        } else {
            downheap(pos);
            upheap(pos);
        }
    }
        
    void remove(const Entry& h) noexcept {
        const auto pos = interface.get_heap_position(h);
        if (pos == TriFace::INVALID) {
            return;
        }
        reset_heap_position(h);
        if (pos + 1 == Base::size()) {
            Base::pop_back();
        } else {
            set_entry(pos, entry(static_cast<uint32_t>(Base::size() - 1)));
            Base::pop_back();
            downheap(pos);
            upheap(pos);
        }
    }

  private:
    void upheap(uint32_t idx) noexcept {
        const Entry h = entry(idx);
        uint32_t par_idx;
        while (idx > 0 && interface.less(h, entry(par_idx = parent(idx)))) {
            set_entry(idx, entry(par_idx));
            idx = par_idx;
        }
        set_entry(idx, h);
    }

    void downheap(uint32_t idx) noexcept {
        const auto h = entry(idx);
        while (idx < Base::size()) {
            auto child_idx = left(idx);
            if (child_idx >= Base::size()) {
                break;
            }
            if (child_idx + 1 < Base::size() && interface.less(entry(child_idx + 1), entry(child_idx))) {
                child_idx += 1;
            }
            if (interface.less(h, entry(child_idx))) {
                break;
            }
            set_entry(idx, entry(child_idx));
            idx = child_idx;
        }
        set_entry(idx, h);
    }

    const Entry& entry(const uint32_t idx) const noexcept { return Base::operator[](idx); }

    void set_entry(const uint32_t idx, const Entry& e) noexcept {
        Base::operator[](idx) = e;
        interface.set_heap_position(e, idx);
    }

    static uint32_t parent(const uint32_t idx) noexcept { return (idx - 1) >> 1; }
    static uint32_t left(const uint32_t idx) noexcept { return (idx << 1) + 1; }
    static uint32_t right(const uint32_t idx) noexcept { return (idx << 1) + 2; }
};
