#pragma once
#include <Eigen/Dense>
#include <array>
#include <vector>

struct MinimumAreaBox {
    struct Box {
        std::array<Eigen::Vector2d, 2> axes;
        std::array<uint32_t, 4> index;
        double area;
        Box() {}
        Box(const double* p, const double* q, const uint32_t idx);
    };

    const double* points;
    std::vector<uint32_t> hull;
    Box min_box;
    MinimumAreaBox(const double* p, std::vector<uint32_t>&& hull);
        
    const double* point(const uint32_t idx) const noexcept { return &points[idx << 1]; }
    void operator()();

  private:
    Box compute_box(const uint32_t e0, const uint32_t e1);
    uint32_t compute_angles(const Box& box, std::pair<double, uint32_t>* angles);
    bool update_support(
        const std::pair<double, uint32_t>* angles, const uint32_t n_angles, std::vector<bool>& visited, Box& box
    );
};
