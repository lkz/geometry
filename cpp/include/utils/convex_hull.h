#pragma once

#include <cstdint>
#include <vector>

void convex_hull(const double* points, const uint32_t n_points, std::vector<double>& hull);
