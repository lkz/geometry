#pragma once

#include <cstdint>
#include <vector>

void collapse_defective_edges_and_faces(
    std::vector<double>& points, std::vector<double>& axes, std::vector<std::vector<uint32_t>>& loops,
    std::vector<std::vector<uint32_t>>& faces, const double tol
);
